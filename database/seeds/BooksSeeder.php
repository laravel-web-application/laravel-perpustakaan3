<?php

use App\Book;
use App\BorrowLog;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = [
            ['title' => 'Tips Menjadi Android Developer', 'author_id' => 1, 'amount' => 2500],
            ['title' => 'Eminem Phonomenal Book', 'author_id' => 1, 'amount' => 2500],
            ['title' => 'Ultraviolet Developer Company', 'author_id' => 1, 'amount' => 2500]
        ];
// masukkan data ke database
        DB::table('books')->insert($books);
        // Sample peminjaman buku
        $member = User::where('email', 'uzumaki_naruto@gmail.com')->first();
        $book = Book::where('email', 'uzumaki_naruto@gmail.com')->first();
        BorrowLog::create(['user_id' => $member->id, 'book_id' => $book1->id, 'is_returned' => 0]);
        BorrowLog::create(['user_id' => $member->id, 'book_id' => $book2->id, 'is_returned' => 0]);
        BorrowLog::create(['user_id' => $member->id, 'book_id' => $book3->id, 'is_returned' => 1]);
    }
}
